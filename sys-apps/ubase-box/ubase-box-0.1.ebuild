# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils 

DESCRIPTION="Suckless alternative to util-linux, written in C."
HOMEPAGE="http://core.suckless.org/ubase"
SRC_URI="http://dl.suckless.org/ubase/ubase-${PV}.tar.gz"

LICENSE="MIT/X"
SLOT="0"
KEYWORDS="~amd64 ~x86"
DEPEND="virtual/pkgconfig"
S="${WORKDIR}/ubase-0.1"

src_compile() {
	emake
	emake ubase-box
}


src_install() {
	into /
	dobin ubase-box
}
