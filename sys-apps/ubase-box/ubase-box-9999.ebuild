# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils git-r3

DESCRIPTION="Suckless alternative to util-linux, written in C."
HOMEPAGE="http://core.suckless.org/ubase"
EGIT_REPO_URI="http://git.suckless.org/ubase"

LICENSE="MIT/X"
SLOT="0"
KEYWORDS=""
DEPEND="virtual/pkgconfig"

src_compile() {
	emake
	emake ubase-box
}


src_install() {
	into /
	dobin ubase-box
}
