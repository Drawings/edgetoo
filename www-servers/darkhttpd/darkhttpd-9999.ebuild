# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils git-r3

DESCRIPTION="A simple, static, fast webserver written in C."
HOMEPAGE="https://unix4lyfe.org/darkhttpd/"
EGIT_REPO_URI="http://unix4lyfe.org/git/darkhttpd"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
DEPEND="virtual/pkgconfig"

src_install() {
	into /usr
	dobin darkhttpd
}
